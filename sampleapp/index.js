var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var vehicle =require('./api.js');	

//var routes = require("./routes/routes.js")(app);
app.use('/getvehicle', vehicle.getVehicle)
app.use('/getvehiclefile', vehicle.getVehicleFile)

var server = app.listen(3000, function () {
    console.log("Listening on port %s...", server.address().port);
});
